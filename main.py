# -*- coding: utf-8 -*-
"""
Celem zadania jest przeprowadzenie analizy, grupowania i klasyfikacji na otrzymanym
zbiorze danych z wykorzystaniem poznanych metod i narzędzi. W szczególności wykonaj
następujące działania:

1.
Z- Określ liczbę obiektów,
Z - liczbę klas,
Z - zakresy zmienności poszczególnych atrybutów,
    Z - ich wartości średnie
    Z - odchylenia standardowe dla całego zbioru i
    - w poszczególnych klasach.
- Wskaż atrybuty o największej i najmniejszej zmienności zgodnie z:
    Z - miarami rozstępu i - najmniejszy rozstep: attr1, najwiekszy: attr2
    Z - odchylenia standardowego. - najmniejsze odchylenie standardowe - attr3, najw: attr2

- Jakie wnioski możesz wyciągnąć z tej analizy ?

2. Oceń wizualnie (
Z - analizując wykresy punktowe,
Z - macierz wykresów punktowych)
 czy podział na grupy reprezentowany przez atrybut decyzyjny odpowiada naturalnym
skupieniom danych w przestrzeni atrybutów. - TAK

3.
Oceń czy w zbiorze danych występują punkty oddalone.
    Jeśli tak to zdecyduj co z nimi zrobić.
Sprawdź czy atrybuty wymagają normalizacji lub standaryzacji.
    Jeśli tak, to wykonaj ją.

4. Przetestuj dostępne klasyfikatory przyjmując sensowną miarę jakości klasyfikacji.
Znajdź najlepszy klasyfikator oraz (jeśli będzie to zasadne) podzbiór zbioru
atrybutów, dla którego klasyfikacja jest najskuteczniejsza.

5. Dokonaj grupowania danych pomijając atrybut decyzyjny. Wykonaj grupowanie dla
różnych liczb grup, znajdź optymalną liczbę grup (przyjmując sensowne kryterium).
Czy w procesie grupowania konieczne jest wykorzystanie wszystkich atrybutów, czy
wystarczy wybrać ich podzbiór ? Czy otrzymany podział jest zgodny z podziałem na
klasy uwidocznionym w atrybucie decyzyjnym ? Oceń to wizualnie (czy jest zgodny z
wnioskami otrzymanymi w punkcie 2) oraz sprawdź numerycznie obliczając stosowne
miary.
"""


###################
####RAFAŁ DOŁEGA###
###################
import statistics
import numpy
from past.builtins import reduce
from plot1 import draw_3d, matrix_chart, draw_histogram, draw_3d_with_distinct

f = open("test1.dat",'r')

def print_data(f):
    for line in f:
        print(line)

def print_rows(array):
    for elem in array:
        print(elem)

def get_header(file):
    for f in file:
        return f.split(" ")

def convert_data_to_array(file):
    """
    Funkcja do konwertowania danych na konkretne listy
    W tej funkcji z pliku test1.dat konwertujemy dane do:
     - listy atrybutów kolumny 1
     - listy atrybutów kolumny 2
     - listy atrybutów kolumny 3
     - listy atrybutów klasy A
     - listy atrybutów klasy B
     - listy atrybutów klasy C
     - listy atrybutów klasy D

    """
    all_data = []
    all_data_without_class = []
    amount_class_A = 0
    amount_class_B = 0
    amount_class_C = 0
    amount_class_D = 0
    attr1_array = []
    attr2_array = []
    attr3_array = []
    attr4_array = []
    attrA_array = []
    attrB_array = []
    attrC_array = []
    attrD_array = []

    for nr, line in enumerate(file):
        if nr==0:
            continue

        line_data = line.split(" ")

        attr1 = float(line_data[1])
        attr2 = float(line_data[2])
        attr3 = float(line_data[3])
        attr4 = line_data[4].strip("\n")
        attr4 = attr4.strip("\"")

        data_without_class =[nr, attr1,attr2,attr3]
        data = [attr1,attr2,attr3,attr4]

        if attr4=='A':
            amount_class_A+=1
            attrA_array.append([attr1,attr2,attr3])
        elif attr4=='B':
            amount_class_B+=1
            attrB_array.append([attr1,attr2,attr3])
        elif attr4=='C':
            amount_class_C+=1
            attrC_array.append([attr1,attr2,attr3])
        elif attr4=='D':
            amount_class_D+=1
            attrD_array.append([attr1,attr2,attr3])
        else:
            print("error")

        attr1_array.append(attr1)
        attr2_array.append(attr2)
        attr3_array.append(attr3)
        attr4_array.append(attr4)

        all_data_without_class.append(data_without_class)
        all_data.append(data)

    return all_data, \
           all_data_without_class, \
           attr1_array, \
           attr2_array, \
           attr3_array, \
           attr4_array, \


def get_class_amount(data):
    """
    Funkcja pomocnicza zwracająca ilości wystąpień danej klasy
    """
    amount_class_A = 0
    amount_class_B = 0
    amount_class_C = 0
    amount_class_D = 0
    for row in data:
        if row[3]=='A':
            amount_class_A+=1
        elif row[3]=='B':
            amount_class_B+=1
        elif row[3]=='C':
            amount_class_C+=1
        elif row[3]=='D':
            amount_class_D+=1
        else:
            print("error")
    return amount_class_A, amount_class_B, amount_class_C, amount_class_D

def get_class_attrs(data):
    """
    Funkcja pomocnicza zwracająca atrybuty dla danej klasy na podstawie pliku z danymi
    """
    attrs_A = []
    attrs_B = []
    attrs_C = []
    attrs_D = []
    for row in data:
        if row[3] == 'A':
            attrs_A.append(row)
        elif row[3]=='B':
            attrs_B.append(row)
        elif row[3]=='C':
            attrs_C.append(row)
        elif row[3]=='D':
            attrs_D.append(row)
    return attrs_A, attrs_B, attrs_C, attrs_D

##########################
####ZADANIE NR.1 #########
##########################
##########################


def object_count(data):
    return len(data)

def get_avg_from_array(list):
    """
    Funkcja zwracająa średnią z listy. 1 sposób
    """
    return (reduce(lambda x, y: x + y, list) / len(list))

def get_avg_from_array_own(list):
    """
    Funkcja zwracająa średnią z listy. 2 sposób
    """
    suma = 0
    for element in list:
        suma+=element
    return suma/len(list)

def get_min_from_array(list):
    return min(list)

def get_max_from_array(list):
    return max(list)

def get_standard_deviations_from_array(list):
    """
    Funkcja zwracająca standardowe odchylenie danej listy. Sposób nr.1
    Metoda skopiowana z internetu
    """
    return statistics.stdev(list)

def get_standard_deviations_from_array_own(array):
    """
    Funkcja zwracająca standardowe odchylenie danej listy. Sposób nr.2
    Metoda zaimplementowana ręcznie
    """
    avg = get_avg_from_array(array)
    suma = 0
    for elem in array:
        suma+= (elem-avg)**2
    return numpy.math.sqrt(suma/len(array))

#########################
####ZADANIE NR.2#########
#### DISTINCTS ##########
#########################

def get_single_coords_from_class_attributes(attrs):
    """
    Metoda zwracająca współrzędne: x,y,z dla danego wiersza danej klasy
    """
    x,y,z = get_single_cords(attrs)
    return x,y,z

def remove_distant_points(array_x, array_y, array_z):
    """
    1.  Każdemu z elementu danej listy współrzędnych atrybutów przypisujemy index
        za pomocą metody "return_with_index".
        Index jest przypisywany do każdego elementu dlatego, żę nawet jeśli jedna z trzech
        współrzędnych jest odstająca - odrzucamy cały punkt:
        Przykład:
            - x był odstający i wystąpił na pozycji nr. 56 (na nieposortowanej liście - liście wyjściowej)
            - y, z nie były odstające
        odrzucany jest cały punkt (X,Y,Z) czyli "x,y,z" o indeksie nr. 56

    2.  Jeśli mamy tę samą listę wyjściową ale z przypisanymi indeksami możemy przystąpić do weryfikacji
        punktów oddalonych metodą "remove_distnts"

    3.  Każdy oddalony punkt w danej osi zapisujemy do wspólnej listy "all_distincts"
        - Lista zawiera punkty oddalone TYLKO Z JEDNEJ KLASY

    """
    def return_with_index(array):
        """
        metoda zwraca listę, którą otrzymuje w argumencie, ale z przypisanymi indeksami
        kolejność ( [element, numer_indexu] ) nie jest przypadkowa.
        Jeśli pierwszy w liście jest element i wywołamy na nim metodę "sort"
        tablica zostanie posortowana po pierwszym elemencie jednocześnie zachowując indeks
        """
        array_index = []
        for number, elem in enumerate(array):
            array_index.append([elem, number])
        return array_index

    array_x_index = return_with_index(array_x)
    array_y_index = return_with_index(array_y)
    array_z_index = return_with_index(array_z)

    array_x_distincts = remove_distants(array_x_index)
    array_y_distincts = remove_distants(array_y_index)
    array_z_distincts = remove_distants(array_z_index)

    all_distinct = []
    for elem in array_x_distincts:
        if elem not in all_distinct:
            all_distinct.append(elem)
    for elem in array_y_distincts:
        if elem not in all_distinct:
            all_distinct.append(elem)
    for elem in array_z_distincts:
        if elem not in all_distinct:
            all_distinct.append(elem)

    return all_distinct

def remove_distants(array):
    """
    Parametr "array": lista w której szukamy punktów oddalonych.
                        Lista atrybutów o danej współrzędnej danej klasy
    1. sortujemy tablicę po wartościach
    2. tworzymy warunki punktu oddalonego na podstawie kwartyli
    3. iterujemy po liście, którą otrzymaliśmy jako parametr
        - jeśli jest mniejsza od dolnego ograniczenia oraz większa od górnego ograncizenia:
            - przypisujemy dany punkt do tablicy "niepasujace", która przechowuje atrybuty, które są oddalone
    """

    tablica_sorted = sorted(array)
    tablica_len = len(tablica_sorted)

    q1_index = int(tablica_len / 4)
    q3_index = int(tablica_len * 3 / 4)
    q1 = tablica_sorted[q1_index][0]
    q3 = tablica_sorted[q3_index][0]
    rozstep = q3- q1
    dolne_ograniczenie = q1 - 1.5 * (rozstep)
    gorne_ograniczenie = q3 + 1.5 * (rozstep)

    niepasujace = []

    for element in tablica_sorted:
        if element[0] < dolne_ograniczenie or element[0] > gorne_ograniczenie:
            niepasujace.append(element)

    return niepasujace

def get_distincts_attributes(attr_distincts, attributes):
    """
    1. funkcja dostaje listę atrybutów odstających z wartością i indeksem oraz oryginalną
        listę atrybutów
    2. iterując po punktach oddalonych do listy 'attrs' przypisujemy cały punkt ( współrzędne x,y,z)
        na podstawie indeksu, który został przekazany.

    attributes[attr[1]][:3] - oznacza wzięcie 3 współrzędnych [:3] z listy attributes na
    indeksie attr[1] (indeks)

    funkcja zwraca listę oddalonych punktów dla danej klasy
    :param attr_distincts:  - lista atrybutów oddalonych
    :param attributes:  - lista oryginalnych atrybutów dla danej klasy
    :return:
    """
    attrs = []

    for attr in attr_distincts:
        attrs.append(attributes[attr[1]][:3])

    return attrs

### DISTINCTS UTILS
def get_one_coords(array, index):
    coords = []
    for a in array:
        coords.append(a[index])
    return coords

def get_single_cords(attrs):
    xx = get_one_coords(attrs, 0)
    yy = get_one_coords(attrs, 1)
    zz = get_one_coords(attrs, 2)
    return xx, yy, zz
#########################


def normalizacja(single_coord_elements):
    """
    metoda liczy normalizację na podstawi listy atrybutów danej współrzędnej dla danej klasy
    :param single_coord_elements:
    :return:
    """
    min = get_min_from_array(single_coord_elements)
    max = get_max_from_array(single_coord_elements)

    normalized_array = []

    for elem in single_coord_elements:
        normalized_array.append(
            (elem-min)/(max-min)
        )
    minn = get_min_from_array(normalized_array)
    maxn = get_max_from_array(normalized_array)

    return normalized_array, minn, maxn


#############################
#######ZADANIE NR. 3#########
######STANDARYZACJA##########
#############################


def check_to_standaryzacja(single_coord_elements):
    """
    metoda, któ®a na podstawie listy jednej współrzędnej jednej klasy
    wylicza standaryzacje
    """
    odchylenie1 = get_standard_deviations_from_array(single_coord_elements)
    odchylenie2 = get_standard_deviations_from_array_own(single_coord_elements)

    avg = get_avg_from_array(single_coord_elements)

    standarized_array = []
    for elem in single_coord_elements:
        standarized_array.append(
            (elem - avg)/odchylenie1
        )
    return standarized_array

def standarize(attr, tostandarize):
    """
    metoda, która standaryzuje daną listę atrybutów
    """
    standarized = []
    for nr, elem in enumerate(attr):
        if tostandarize[nr] >-1 and tostandarize[nr]<1:
            standarized.append(elem)

    return standarized

data, data_without, attr1_array, attr2_array, attr3_array, attr4_array = convert_data_to_array(f)

avg1 = get_avg_from_array(attr1_array)
avg2 = get_avg_from_array(attr2_array)
avg3 = get_avg_from_array(attr3_array)

min1 = get_min_from_array(attr1_array)
max1 = get_max_from_array(attr1_array)

min2 = get_min_from_array(attr2_array)
max2 = get_max_from_array(attr2_array)

min3 = get_min_from_array(attr3_array)
max3 = get_max_from_array(attr3_array)

attrs_A, attrs_B, attrs_C, attrs_D = get_class_attrs(data)
attrsAx, attrsAy, attrsAz = get_single_coords_from_class_attributes(attrs_A)
attrsBx, attrsBy, attrsBz = get_single_coords_from_class_attributes(attrs_B)
attrsCx, attrsCy, attrsCz = get_single_coords_from_class_attributes(attrs_C)
attrsDx, attrsDy, attrsDz = get_single_coords_from_class_attributes(attrs_D)


avgAx = get_avg_from_array_own(attrsAx)
avgAy = get_avg_from_array_own(attrsAy)
avgAz = get_avg_from_array_own(attrsAz)
avgBx = get_avg_from_array_own(attrsBx)
avgBy = get_avg_from_array_own(attrsBy)
avgBz = get_avg_from_array_own(attrsBz)
avgCx = get_avg_from_array_own(attrsCx)
avgCy = get_avg_from_array_own(attrsCy)
avgCz = get_avg_from_array_own(attrsCz)
avgDx = get_avg_from_array_own(attrsDx)
avgDy = get_avg_from_array_own(attrsDy)
avgDz = get_avg_from_array_own(attrsDz)

minAx = get_min_from_array(attrsAx)
minAy = get_min_from_array(attrsAy)
minAz = get_min_from_array(attrsAz)
minBx = get_min_from_array(attrsBx)
minBy = get_min_from_array(attrsBy)
minBz = get_min_from_array(attrsBz)
minCx = get_min_from_array(attrsCx)
minCy = get_min_from_array(attrsCy)
minCz = get_min_from_array(attrsCz)
minDx = get_min_from_array(attrsDx)
minDy = get_min_from_array(attrsDy)
minDz = get_min_from_array(attrsDz)

maxAx = get_max_from_array(attrsAx)
maxAy = get_max_from_array(attrsAy)
maxAz = get_max_from_array(attrsAz)
maxBx = get_max_from_array(attrsBx)
maxBy = get_max_from_array(attrsBy)
maxBz = get_max_from_array(attrsBz)
maxCx = get_max_from_array(attrsCx)
maxCy = get_max_from_array(attrsCy)
maxCz = get_max_from_array(attrsCz)
maxDx = get_max_from_array(attrsDx)
maxDy = get_max_from_array(attrsDy)
maxDz = get_max_from_array(attrsDz)


amount_class_A, amount_class_B, amount_class_C, amount_class_D = get_class_amount(data)

print("Liczba obiektów lasy A: {}".format(amount_class_A))
print("Liczba obiektów lasy B: {}".format(amount_class_B))
print("Liczba obiektów lasy C: {}".format(amount_class_C))
print("Liczba obiektów lasy D: {}".format(amount_class_D))

print("Odchylenie standardowe atrybutu X dla całego zbioru: {}".format(get_standard_deviations_from_array(attr1_array)))
print("Odchylenie standardowe atrybutu Y dla całego zbioru: {}".format(get_standard_deviations_from_array(attr2_array)))
print("Odchylenie standardowe atrybutu Z dla całego zbioru: {}".format(get_standard_deviations_from_array(attr3_array)))
print("Odchylenie standardowe atrybutu X dla klasy A: {}".format(get_standard_deviations_from_array(attrsAx)))
print("Odchylenie standardowe atrybutu Y dla klasy A: {}".format(get_standard_deviations_from_array(attrsAy)))
print("Odchylenie standardowe atrybutu Z dla klasy A: {}".format(get_standard_deviations_from_array(attrsAz)))
print("Odchylenie standardowe atrybutu X dla klasy B: {}".format(get_standard_deviations_from_array(attrsBx)))
print("Odchylenie standardowe atrybutu Y dla klasy B: {}".format(get_standard_deviations_from_array(attrsBy)))
print("Odchylenie standardowe atrybutu Z dla klasy B: {}".format(get_standard_deviations_from_array(attrsBz)))
print("Odchylenie standardowe atrybutu X dla klasy C: {}".format(get_standard_deviations_from_array(attrsCx)))
print("Odchylenie standardowe atrybutu Y dla klasy C: {}".format(get_standard_deviations_from_array(attrsCy)))
print("Odchylenie standardowe atrybutu Z dla klasy C: {}".format(get_standard_deviations_from_array(attrsCz)))
print("Odchylenie standardowe atrybutu X dla klasy D: {}".format(get_standard_deviations_from_array(attrsDx)))
print("Odchylenie standardowe atrybutu Y dla klasy D: {}".format(get_standard_deviations_from_array(attrsDy)))
print("Odchylenie standardowe atrybutu Z dla klasy D: {}".format(get_standard_deviations_from_array(attrsDz)))

print("Zakres zmienności atrybut X całego zbioru: Min: {}, Max: {}, Avg: {}.".format(min1, max1, avg1))
print("Zakres zmienności atrybut Y całego zbioru: Min: {}, Max: {}, Avg: {}.".format(min2, max2, avg2))
print("Zakres zmienności atrybut Z całego zbioru: Min: {}, Max: {}, Avg: {}.".format(min3, max3, avg3))
print("Zakres zmienności atrybut X klasy A: Min: {}, Max: {}, Avg: {}.".format(minAx, maxAx, avgAx))
print("Zakres zmienności atrybut Y klasy A: Min: {}, Max: {}, Avg: {}.".format(minAy, maxAy, avgAy))
print("Zakres zmienności atrybut Z klasy A: Min: {}, Max: {}, Avg: {}.".format(minAz, maxAz, avgAz))
print("Zakres zmienności atrybut X klasy B: Min: {}, Max: {}, Avg: {}.".format(minBx, maxBx, avgBx))
print("Zakres zmienności atrybut Y klasy B: Min: {}, Max: {}, Avg: {}.".format(minBy, maxBy, avgBy))
print("Zakres zmienności atrybut Z klasy B: Min: {}, Max: {}, Avg: {}.".format(minBz, maxBz, avgBz))
print("Zakres zmienności atrybut X klasy C: Min: {}, Max: {}, Avg: {}.".format(minCx, maxCx, avgCx))
print("Zakres zmienności atrybut Y klasy C: Min: {}, Max: {}, Avg: {}.".format(minCy, maxCy, avgCy))
print("Zakres zmienności atrybut Z klasy C: Min: {}, Max: {}, Avg: {}.".format(minCz, maxCz, avgCz))
print("Zakres zmienności atrybut X klasy D: Min: {}, Max: {}, Avg: {}.".format(minDx, maxDx, avgDx))
print("Zakres zmienności atrybut Y klasy D: Min: {}, Max: {}, Avg: {}.".format(minDy, maxDy, avgDy))
print("Zakres zmienności atrybut Z klasy D: Min: {}, Max: {}, Avg: {}.".format(minDz, maxDz, avgDz))

print("Miara rozstępu atrybutu X dla całego zbioru: {}".format(max1-min1))
print("Miara rozstępu atrybutu Y dla całego zbioru: {}".format(max2-min2))
print("Miara rozstępu atrybutu Z dla całego zbioru: {}".format(max3-min3))

print("Miara rozstępu atrybutu X dla klasy A: {}".format(maxAx-minAx))
print("Miara rozstępu atrybutu Y dla klasy A: {}".format(maxAy-minAy))
print("Miara rozstępu atrybutu Z dla klasy A: {}".format(maxAz-minAz))
print("Miara rozstępu atrybutu X dla klasy B: {}".format(maxBx-minBx))
print("Miara rozstępu atrybutu Y dla klasy B: {}".format(maxBy-minBy))
print("Miara rozstępu atrybutu Z dla klasy B: {}".format(maxBz-minBz))
print("Miara rozstępu atrybutu X dla klasy C: {}".format(maxCx-minCx))
print("Miara rozstępu atrybutu Y dla klasy C: {}".format(maxCy-minCy))
print("Miara rozstępu atrybutu Z dla klasy C: {}".format(maxCz-minCz))
print("Miara rozstępu atrybutu X dla klasy D: {}".format(maxDx-minDx))
print("Miara rozstępu atrybutu Y dla klasy D: {}".format(maxDy-minDy))
print("Miara rozstępu atrybutu Z dla klasy D: {}".format(maxDz-minDz))

# rysowanie wykresów histogram
draw_histogram(attr1_array)
draw_histogram(attr2_array)
draw_histogram(attr3_array)

# rysowanie wykresu macierzy punktowej
matrix_chart(data_without)

# 3d po klasach
draw_3d(attrs_A, attrs_B, attrs_C, attrs_D)


attrsAd_disctints = remove_distant_points(attrsAx, attrsAy, attrsAz)
attrsBd_disctints = remove_distant_points(attrsBx, attrsBy, attrsBz)
attrsCd_disctints = remove_distant_points(attrsCx, attrsCy, attrsCz)
attrsDd_disctints = remove_distant_points(attrsDx, attrsDy, attrsDz)

attrsAd = get_distincts_attributes(attrsAd_disctints, attrs_A)
attrsBd = get_distincts_attributes(attrsBd_disctints, attrs_B)
attrsCd = get_distincts_attributes(attrsCd_disctints, attrs_C)
attrsDd = get_distincts_attributes(attrsDd_disctints, attrs_D)

# odstajace 3d
draw_3d_with_distinct(attrs_A, attrs_B, attrs_C, attrs_D,
                      attrsAd, attrsBd, attrsCd, attrsDd)

normalized_attr1 = normalizacja(attr1_array)
normalized_attr2 = normalizacja(attr2_array)
normalized_attr3 = normalizacja(attr3_array)
standarized_attr1 = check_to_standaryzacja(attr1_array)
standarized_attr2 = check_to_standaryzacja(attr2_array)
standarized_attr3 = check_to_standaryzacja(attr3_array)

standarized_attr1_result = standarize(attr1_array, standarized_attr1)
standarized_attr2_result = standarize(attr2_array, standarized_attr2)
standarized_attr3_result = standarize(attr3_array, standarized_attr3)

###########################
######PAWEŁ WYSMYK#########
###########################


import statistics
import math
import numpy
import pandas as pd
from matplotlib import pyplot as plt
import plotly.plotly as py
import plotly.graph_objs as go
import plotly

def get_var_from_array(list):
    return statistics.variance(list)

def sum_list(list):
    sum = 0
    for i in list:
        sum += i
    return sum

def highest_score(a,b,c,d):
    list = [a,b,c,d]
    max = get_max_from_array(list)
    position = list.index(max)
    if position == 0:
        position = 'A'
    elif position == 1:
        position = 'B'
    elif position == 2:
        position = 'C'
    else:
        position = 'D'
    return max, position

#Funckja zwracajaca prawdopodobienstwo
def probab(data,avg, div):
    p = 0.0
    p = float(1/math.sqrt(2*math.pi*div))*math.exp((-(data-avg)**2)/(2*div))
    return p
#Zadanie 4
#Funkcja wydziela listy dla poszczegolnych klas, liczy srednie, variancje, oraz czesci
def list_for_class(array):

    attrX_array = []
    attrY_array = []
    attrZ_array = []

    for line in array:
        attr1 = line[0]
        attr2 = line[1]
        attr3 = line[2]
        attrX_array.append(attr1)
        attrY_array.append(attr2)
        attrZ_array.append(attr3)

    avgX = get_avg_from_array(attrX_array)
    avgY = get_avg_from_array(attrY_array)
    avgZ = get_avg_from_array(attrZ_array)

    devX = get_var_from_array(attrX_array)
    devY = get_var_from_array(attrY_array)
    devZ = get_var_from_array(attrZ_array)

    total1 = sum_list(attr1_array)
    total2 = sum_list(attr2_array)
    total3 = sum_list(attr3_array)

    sum1 = sum_list(attrX_array)
    sum2 = sum_list(attrY_array)
    sum3 = sum_list(attrZ_array)

    sumX = sum1 / total1
    sumY = sum2 / total2
    sumZ = sum3 / total3

    return avgX, avgY, avgZ, devX, devY, devZ, sumX, sumY, sumZ

#1
#Tworzenie apriori dla klas
priorisum = amount_class_A + amount_class_B + amount_class_C + amount_class_D
aprioriA = float(amount_class_A) / priorisum
aprioriB = float(amount_class_B) /priorisum
aprioriC = float(amount_class_C) /priorisum
aprioriD = float(amount_class_D) /priorisum

#3
#Dane z klas
avgXA, avgYA, avgZA, devXA, devYA, devZA, sumXA, sumYA, sumZA= list_for_class(attrs_A)
avgXB, avgYB, avgZB, devXB, devYB, devZB, sumXB, sumYB, sumZB = list_for_class(attrs_B)
avgXC, avgYC, avgZC, devXC, devYC, devZC, sumXC, sumYC, sumZC = list_for_class(attrs_C)
avgXD, avgYD, avgZD, devXD, devYD, devZD, sumXD, sumYD, sumZD = list_for_class(attrs_D)
#dane testowe
test_x = 5.02843
test_y = 34.7876
test_z = 0.9156

#Prawdopodobienstwa, Kalsyfikacja naiwym Bayesem
PXA = probab(test_x, avgXA, devXA)
PYA = probab(test_y, avgYA, devYA)
PZA = probab(test_z, avgZA, devZA)

PXB = probab(test_x, avgXB, devXB)
PYB = probab(test_y, avgYB, devYB)
PZB = probab(test_z, avgZB, devZB)

PXC = probab(test_x, avgXC, devXC)
PYC = probab(test_y, avgYC, devYC)
PZC = probab(test_z, avgZC, devZC)

PXD = probab(test_x, avgXD, devXD)
PYD = probab(test_y, avgYD, devYD)
PZD = probab(test_z, avgZD, devZD)

#Posteriori
posteriorA = aprioriA*PXA*PYA*PZA
posteriorB = aprioriB*PXB*PYB*PZB
posteriorC = aprioriC*PXC*PYC*PZC
posteriorD = aprioriD*PXD*PYD*PZD

evidence = posteriorA + posteriorB + posteriorC + posteriorD

#Prawdopodobienstwa klasyfikacji
wynikA = posteriorA /evidence
wynikB = posteriorB / evidence
wynikC = posteriorC / evidence
wynikD = posteriorD / evidence

print(wynikA, wynikB, wynikC, wynikD)
score, klasa = highest_score(wynikA, wynikB, wynikC, wynikD)

print('Najwyższy wynik to {}.'.format(score))
print('Wyniknależy do klasy {}.'.format(klasa))

#5 Grupowanie

#Pogrupuj dane
#Pogrupuj dla różnych liczba grup
#Zwizualizuj

plotly.tools.set_credentials_file(username='wysmyq', api_key='yGbvP9QgFZB1dAZzRVQD')
plt.rcParams['figure.figsize'] = (16, 9)
plt.style.use('ggplot')

# Importing the dataset
df = pd.read_table('test1.dat', delim_whitespace=True)
df.head()
#df.drop(df.index[300:1104], axis = 0, inplace=True)
df.drop("klasa", axis = 1, inplace = True )


print(df)
data = []
clusters = []

for i in df:
    x = df['atrybut1']
    y = df['atrybut2']
    z = df['atrybut3']

    trace = dict(
        name="PLOT",
        x=x, y=y, z=z,
        type="scatter3d",
        mode='markers',
        marker=dict(size=3, line=dict(width=0)))
    data.append(trace)

layout = go.Layout(
    width=1000,
    height=1000,
    autosize=False,
    title='Eksploracja punkty',
    scene=dict(
        xaxis=dict(
            gridcolor='rgb(255, 255, 255)',
            zerolinecolor='rgb(255, 255, 255)',
            showbackground=True,
            backgroundcolor='rgb(230, 230,230)'
        ),
        yaxis=dict(
            gridcolor='rgb(255, 255, 255)',
            zerolinecolor='rgb(255, 255, 255)',
            showbackground=True,
            backgroundcolor='rgb(230, 230,230)'
        ),
        zaxis=dict(
            gridcolor='rgb(255, 255, 255)',
            zerolinecolor='rgb(255, 255, 255)',
            showbackground=True,
            backgroundcolor='rgb(230, 230,230)'
        ),
        aspectratio = dict( x=1, y=1, z=0.7 ),
        aspectmode = 'manual'
    )
)

fig = dict(data=data, layout=layout)

# Zwizualizuj wykres punktowy 3D danych

url = py.plot(fig, filename='Eksploracja punkty')

#Plot dla dwóch atrybutów
# Create a trace
trace = go.Scatter(
    x = df['atrybut1'],
    y = df['atrybut3'],
    mode = 'markers'
)

data = [trace]
plot_url = py.plot(data, filename='basic-line')