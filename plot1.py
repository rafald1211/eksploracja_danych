import plotly.plotly as py
import plotly.graph_objs as go
import matplotlib.pyplot as plt
import plotly.plotly as py
import plotly.figure_factory as ff

import numpy as np
import pandas as pd
from numpy import array
import plotly.plotly as py
# Learn about API authentication here: https://plot.ly/python/getting-started
# Find your api_key here: https://plot.ly/settings/api
import plotly.graph_objs as go

def draw_histogram(array):
    """
    metoda wyświetlająca wykres histogram dla danego wiersza danych
    """
    x = array
    data = [go.Histogram(x=x,histnorm='probability')]
    py.plot(data, filename='basic histogram')


def matrix_chart(data):
    """
    metoda wyświetlająca macierz wykresów
    """
    numpy_data = array(data)
    dataframe = pd.DataFrame(numpy_data,
                             columns=['Index', 'Attribute A', 'Attribute B', 'Attribute C'])

    fig = ff.create_scatterplotmatrix(
        dataframe,
        diag='histogram',
        index='Index',
        colormap=['#000000', '#F0963C', '#FFFFFF'],
        colormap_type='seq', height=800, width=800)
    py.plot(fig, filename = 'Custom Sequential Colormap')


def convert_array_to_numpy_coords(attribute_array):
    x = []
    y = []
    z = []
    for row in attribute_array:
        x.append(row[0])
        y.append(row[1])
        z.append(row[2])
    return array(x), array(y), array(z)


def draw_3d(attrA, attrB, attrC, attrD):
    """
    Metoda wyświetlająca wykres 3d atrybutów należących do danej klasy
    """
    xA,yA,zA = convert_array_to_numpy_coords(attrA)
    xB,yB,zB = convert_array_to_numpy_coords(attrB)
    xC,yC,zC = convert_array_to_numpy_coords(attrC)
    xD,yD,zD = convert_array_to_numpy_coords(attrD)
    # x, y, z = np.random.multivariate_normal(np.array([0, 0, 0]), np.eye(3), 200).transpose()
    trace1 = go.Scatter3d(
        x=xA,
        y=yA,
        z=zA,
        mode='markers',
        marker=dict(
            #light red
            color='#FFCDD2',
            size=12,
            line=dict(
                color='rgba(0, 0, 0, 0.14)',
                width=0.5
            ),
            opacity=0.8
        )
    )

    # x2, y2, z2 = np.random.multivariate_normal(np.array([0, 0, 0]), np.eye(3), 200).transpose()
    trace2 = go.Scatter3d(
        x=xB,
        y=yB,
        z=zB,
        mode='markers',
        marker=dict(
            # light blue
            color='#81D4FA',
            size=12,
            symbol='circle',
            line=dict(
                color='rgb(204, 204, 204)',
                width=1
            ),
            opacity=0.9
        )
    )
    trace3 = go.Scatter3d(
        x=xC,
        y=yC,
        z=zC,
        mode='markers',
        marker=dict(
            # light green
            color='#C8E6C9',
            size=12,
            symbol='circle',
            line=dict(
                color='rgb(250, 250, 250)',
                width=1
            ),
            opacity=0.9
        )
    )
    trace4 = go.Scatter3d(
        x=xD,
        y=yD,
        z=zD,
        mode='markers',
        marker=dict(
            # light yellow
            color='#FFF59D',
            size=12,
            symbol='circle',
            line=dict(
                color='rgb(250, 250, 250)',
                width=1
            ),
            opacity=0.9
        )
    )

    data = [trace1, trace2, trace3, trace4]
    layout = go.Layout(
        margin=dict(
            l=0,
            r=0,
            b=0,
            t=0
        )
    )
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='simple-3d-scatter')



def draw_3d_with_distinct(attrA, attrB, attrC, attrD,
                          attrAdistinct, attrBdistinct,
                          attrCdistinct, attrDdistinct):
    """
    Metoda wyświetlająca wykres 3d atrybutów należących do danej klasy
    Metoda wyświetla również podkreślonym kolorem atrybuty odstające dla danej klasy.
        (Ciemniejszy odcień tego samego koloru)
    """
    xA,yA,zA = convert_array_to_numpy_coords(attrA)
    xB,yB,zB = convert_array_to_numpy_coords(attrB)
    xC,yC,zC = convert_array_to_numpy_coords(attrC)
    xD,yD,zD = convert_array_to_numpy_coords(attrD)
    xAd,yAd,zAd = convert_array_to_numpy_coords(attrAdistinct)
    xBd,yBd,zBd = convert_array_to_numpy_coords(attrBdistinct)
    xCd,yCd,zCd = convert_array_to_numpy_coords(attrCdistinct)
    xDd,yDd,zDd = convert_array_to_numpy_coords(attrDdistinct)


 # x, y, z = np.random.multivariate_normal(np.array([0, 0, 0]), np.eye(3), 200).transpose()
    traceA = go.Scatter3d(
        x=xA,
        y=yA,
        z=zA,
        mode='markers',
        marker=dict(
            # light red
            color='#FFCDD2',
            size=12,
            line=dict(
                color='rgba(0, 0, 0, 0.14)',
                width=0.5
            ),
            opacity=0.8
        )
    )

    # x2, y2, z2 = np.random.multivariate_normal(np.array([0, 0, 0]), np.eye(3), 200).transpose()
    traceB = go.Scatter3d(
        x=xB,
        y=yB,
        z=zB,
        mode='markers',
        marker=dict(
            # light blue
            color='#81D4FA',
            size=12,
            symbol='circle',
            line=dict(
                color='rgb(204, 204, 204)',
                width=1
            ),
            opacity=0.9
        )
    )
    traceC = go.Scatter3d(
        x=xC,
        y=yC,
        z=zC,
        mode='markers',
        marker=dict(
            # light green
            color='#C8E6C9',
            size=12,
            symbol='circle',
            line=dict(
                color='rgb(250, 250, 250)',
                width=1
            ),
            opacity=0.9
        )
    )
    traceD = go.Scatter3d(
        x=xD,
        y=yD,
        z=zD,
        mode='markers',
        marker=dict(
            # light yellow
            color='#FFF59D',
            size=12,
            symbol='circle',
            line=dict(
                color='rgb(250, 250, 250)',
                width=1
            ),
            opacity=0.9
        )
    )
    traceAd = go.Scatter3d(
        x=xAd,
        y=yAd,
        z=zAd,
        mode='markers',
        marker=dict(
            # red
            color='#a83825',
            size=12,
            symbol='circle',
            line=dict(
                color='rgb(250, 250, 250)',
                width=1
            ),
            opacity=0.9
        )
    )
    traceBd = go.Scatter3d(
        x=xBd,
        y=yBd,
        z=zBd,
        mode='markers',
        marker=dict(
            # blue
            color='#01579B',
            size=12,
            symbol='circle',
            line=dict(
                color='rgb(250, 250, 250)',
                width=1
            ),
            opacity=0.9
        )
    )
    traceCd = go.Scatter3d(
        x=xCd,
        y=yCd,
        z=zCd,
        mode='markers',
        marker=dict(
            # green
            color='#1B5E20',
            size=12,
            symbol='circle',
            line=dict(
                color='rgb(250, 250, 250)',
                width=1
            ),
            opacity=0.9
        )
    )
    traceDd = go.Scatter3d(
        x=xDd,
        y=yDd,
        z=zDd,
        mode='markers',
        marker=dict(
            # orange
            color='#F9A825',
            size=12,
            symbol='circle',
            line=dict(
                color='rgb(250, 250, 250)',
                width=1
            ),
            opacity=0.9
        )
    )


    data = [
            traceA, traceB, traceC, traceD,
            traceAd, traceBd, traceCd, traceDd]
    layout = go.Layout(
        margin=dict(
            l=0,
            r=0,
            b=0,
            t=0
        )
    )
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='simple-3d-scatter')
